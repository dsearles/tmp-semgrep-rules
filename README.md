# Semgrep Rules

A place for the semgrep analyzer to store rules with a different license.

## Contributing

This repository does not currently accept community contributions. Once we figure out our process, we will
welcome contributions at https://semgrep.dev/explore. If you want to follow along with our process you can
take a look at https://gitlab.com/gitlab-org/gitlab/-/issues/332244

## License

The rules in the repository is [LGPL
2.1](https://tldrlegal.com/license/gnu-lesser-general-public-license-v2.1-(lgpl-2.1)) in order to match
https://semgrep.dev/explore as they will likely move there sometime in the future.
